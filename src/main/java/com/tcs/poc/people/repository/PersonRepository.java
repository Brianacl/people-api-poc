package com.tcs.poc.people.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tcs.poc.people.model.Person;

@Repository
public interface PersonRepository extends JpaRepository<Person, Integer> {}
