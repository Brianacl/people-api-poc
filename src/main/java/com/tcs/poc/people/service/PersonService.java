package com.tcs.poc.people.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.tcs.poc.people.exception.InternalServerErrorException;
import com.tcs.poc.people.exception.ResourceNotFoundException;
import com.tcs.poc.people.model.Address;
import com.tcs.poc.people.model.Person;
import com.tcs.poc.people.repository.PersonRepository;

@Service
public class PersonService {
	@Autowired
	private PersonRepository personRepository;
	
	@Autowired
	private WebClient.Builder webClientBuilder;
	
	@Async
	public CompletableFuture<List<Person>> getAllPeople() {
		return CompletableFuture.completedFuture(
				this.personRepository.findAll().stream().map(person -> {
					Address addresses[] = webClientBuilder.build()
						.get()
						.uri("http://gateway-service/addresses/api/v1/address?personId=" + person.getId())
						.retrieve()
						.bodyToMono(Address[].class)
						.block();
					
					person.setAddresses(addresses);
					return person;
				})
				.collect(Collectors.toList())
			);
	}
	
	@Async
	public CompletableFuture<Person> getPersonById(int personId) 
		throws ResourceNotFoundException {
		Person person = this.personRepository
				.findById(personId)
				.orElseThrow(
						() -> new ResourceNotFoundException("Person not found on :: " + personId)
						);

		person.setAddresses(
				webClientBuilder.build()
					.get()
					.uri("http://gateway-service/addresses/api/v1/address?personId=" + person.getId())
					.retrieve()
					.bodyToMono(Address[].class)
					.block()
				);
		
		return CompletableFuture.completedFuture(person);
	}
	
	@Async
	public CompletableFuture<Person> createPerson(final Person person) {
		return CompletableFuture.completedFuture(
				this.personRepository.save(person)
				);
	}
	
	@Async
	public CompletableFuture<Person> updatePerson(final int personId, final Person personDetails) 
			throws ResourceNotFoundException {
		Person person = this.personRepository
				.findById(personId)
				.orElseThrow(
						() -> new ResourceNotFoundException("Person not found on :: " + personId)
						);

		person.setFirstName(personDetails.getFirstName());
		person.setLastName(personDetails.getLastName());
		person.setAge(personDetails.getAge());
		person.setBloodGroup(personDetails.getBloodGroup());
		
		return CompletableFuture.completedFuture(this.personRepository.save(person));
	}
	
	@Async
	public CompletableFuture<Map<String, String>> deletePerson(final int personId)
		throws ResourceNotFoundException, InternalServerErrorException {
		Person person = this.personRepository
				.findById(personId)
				.orElseThrow(
						() -> new ResourceNotFoundException("Person not found on:: " + personId)
						);

		person.setAddresses(webClientBuilder.build()
				.get()
				.uri("http://gateway-service/addresses/api/v1/address?personId=" + person.getId())
				.retrieve()
				.bodyToMono(Address[].class)
				.block()
			);

		Map<String, String> response = new HashMap<>();
		
		if(person.getAddresses().length > 0)
			throw new InternalServerErrorException("Some addresses are related to this person.");
		
		this.personRepository.delete(person);
		response.put("deleted", "true");
		return CompletableFuture.completedFuture(response);
		
		
	}
}
