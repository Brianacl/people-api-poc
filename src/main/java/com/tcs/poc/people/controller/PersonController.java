package com.tcs.poc.people.controller;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;

import javax.validation.Valid;

import com.tcs.poc.people.exception.InternalServerErrorException;
import com.tcs.poc.people.exception.ResourceNotFoundException;
import com.tcs.poc.people.model.Person;
import com.tcs.poc.people.service.PersonService;;

@RestController
@RequestMapping("/api/v1")
@Tag(name = "Person", description = "Person API")
public class PersonController {
	
	@Autowired
	private PersonService personService;
	
	@Operation(summary = "Find all people registered with their addresses")
	@GetMapping(value= "/people", produces = { "application/json"})
	public CompletableFuture<ResponseEntity<List<Person>>> getAllPeople() {
		return personService.getAllPeople()
				.<ResponseEntity<List<Person>>>thenApply(ResponseEntity::ok)
				.exceptionally(getPeopleFailure);
	}
	
	private static Function<Throwable,? 
			extends ResponseEntity<List<Person>>> getPeopleFailure = throwable ->
				ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
	
	@Operation(summary = "Find person by id")
	@GetMapping(value = "/person/{id}", produces = { "application/json"})
	public CompletableFuture<ResponseEntity<Person>> getPersonById(
			@Parameter(description="The person id must be provided to find a record (required).", required = true)
			@PathVariable(value = "id") Integer personId
			)
		throws ResourceNotFoundException {
			return personService.getPersonById(personId)
					.<ResponseEntity<Person>>thenApply(ResponseEntity::ok);
	}
	
	@Operation(summary = "Add a new person", tags = {"person"})
	@PostMapping(value = "/person", consumes = { "application/json"})
	public ResponseEntity<Person> createPerson(
			@Parameter(description = "Person to add.",
					required = true, schema = @Schema(implementation = Person.class,
					example = 
					"{ \"firstName\":\"string\", \"lastName\": \"string\", \"age\": 0, \"bloodGroup\": \"O+\"}"))
			@Valid @RequestBody Person person
			) throws InterruptedException, ExecutionException {
		
		return ResponseEntity.status(HttpStatus.CREATED).body(
				this.personService.createPerson(person).get()
				);
	}
	
	@Operation(summary = "Update an existing person", tags = {"person"})
	@PutMapping(value = "/person/{id}", consumes = { "application/json"})
	public CompletableFuture<ResponseEntity<Person>> updatePerson(
			@Parameter(description="Person id required to update. Cannot be empty.", 
            	required=true)
			@PathVariable(value = "id") Integer personId,
			@Parameter(description="Person to update. Cannot null or empty", 
            	required=true, schema=@Schema(implementation = Person.class, 
            	example = "{ \"firstName\":\"string\", \"lastName\": \"string\", \"age\": 0, \"bloodGroup\": \"O+\"}"))
			@Valid @RequestBody Person personDetails
			) throws ResourceNotFoundException {
		
		return this.personService.updatePerson(personId, personDetails)
				.<ResponseEntity<Person>>thenApply(ResponseEntity::ok);
	}
	
	@Operation(summary = "Deletes a person", tags = { "person" })
	@DeleteMapping("/person/{id}")
	public CompletableFuture<ResponseEntity<Map<String, String>>> deletePerson(
			@Parameter(description="Person id required to be deleted. Cannot be empty.",
            	required=true)
			@PathVariable(value = "id") Integer personId
			) throws ResourceNotFoundException, InternalServerErrorException {
		
		return this.personService.deletePerson(personId)
				.<ResponseEntity<Map<String, String>>>thenApply(ResponseEntity::ok);
	}
}
