package com.tcs.poc.people.model;

import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.Column;
import javax.persistence.Entity;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.media.Schema;

@Entity
@Table(name = "people")
@EntityListeners(AuditingEntityListener.class)
public class Person {
	
	@Id
	@Hidden
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Schema(description = "First name of the person.", 
            example = "Brian Alejandro", required = true)
	@NotBlank
	@Size(min = 1, max = 20)
	@Column(name = "first_name", nullable = false, length=20)
	private String firstName;
	
	@Schema(description = "Last name of the person.", 
            example = "Casas López", required = true)
	@NotBlank
	@Size(min = 1, max = 20)
	@Column(name = "last_name", nullable = false, length=20)
	private String lastName;
	
	@Schema(description = "Age of person.", 
            example = "23", required = true)
	@NotBlank
	@Column(name = "age_name", nullable = false)
	private int age;
	
	@Schema(description = "Blood group.", 
            example = "O+", required = true)
	@NotBlank
	@Size(min = 2, max = 3)
	@Column(name = "blood_group", nullable = false, length=3)
	private String bloodGroup;

	/* Tables relation */
	private Address[] addresses;
	
	/*Getters and setters*/
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getBloodGroup() {
		return bloodGroup;
	}

	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}
	
	

	public Address[] getAddresses() {
		return addresses;
	}

	public void setAddresses(Address[] addresses) {
		this.addresses = addresses;
	}

	@Override
	public String toString() {
		return "Person{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age='" + age + '\'' +
                ", bloodGroup=" + bloodGroup +
                '}';
	}
}
